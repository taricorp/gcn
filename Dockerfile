FROM python:3.12-alpine
LABEL org.opencontainers.image.url="https://gitlab.com/taricorp/gcn" \
      org.opencontainers.image.title="Global CALCnet metahub server"

RUN pip install gCn --index-url https://gitlab.com/api/v4/projects/56123403/packages/pypi/simple
ENTRYPOINT ["/usr/local/bin/gcnhub"]
