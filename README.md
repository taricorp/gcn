gCn
===

globalCALCnet hub, bridges, and service-provider bridges

### What is globalCALCnet?

gCn is a method of connecting local-area CALCnet networks over the internet, as
well as connecting calculators to internet services.

### Does it work?

Well, kind of. These servers were written around 2010 and remained mostly
untouched since then. They also weren't designed to be very easily deployed.

This repository is a fork of the original at https://github.com/KermMartian/gCn
and seeks to bring the servers up to a more modern standard. The servers and
their current status:

 * [x] gcnhub: tested with [Calcnet Chat!](https://www.cemetech.net/downloads/files/549)
       and the [CALCnet C# library](https://www.cemetech.net/downloads/files/557)
       demo application, seems to work just fine.
 * [ ] gcnirc: I think I understand the protocol.
 * [ ] gcnguest: doesn't seem worth reimplementing. It appears to have only
       been used for Maker Faire 2014 and depended on an additional web service
       that doesn't seem to exist anywhere.
 * [ ] gcnftp: not started
 * [ ] gcnweb: not started. Safe implementation may be tricky because the
       bridge is basically an open proxy that should ensure clients can't
       reach private resources on the same network as the bridge.