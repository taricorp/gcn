from importlib.metadata import version

from .address import Address
from .endpoint.ip import InternetEndpoint
from .endpoint.dusb import DirectUsbEndpoint

__version__ = version("gcn")
