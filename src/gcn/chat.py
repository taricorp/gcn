import asyncio
import logging
from abc import ABC, abstractmethod
from asyncio import Queue, wait_for, Task, create_task, CancelledError, Event
from dataclasses import dataclass
from enum import IntEnum
from typing import Optional

from gcn import Address
from gcn.endpoint import Endpoint

logger = logging.getLogger(__name__)


class MessageKind(IntEnum):
    PING = 0xAB
    DISCONNECT = 0xAC
    TEXT = 0xAD


@dataclass(frozen=True)
class Message(ABC):
    source: Address
    destination: Address

    @abstractmethod
    def __bytes__(self):
        raise NotImplementedError()  # pragma: nocover


@dataclass(frozen=True)
class PingMessage(Message):
    nickname: str

    def __bytes__(self):
        return (
            bytes((MessageKind.PING,)) + (self.nickname.encode("ascii") + bytes(8))[:8]
        )


@dataclass(frozen=True)
class DisconnectMessage(Message):
    def __bytes__(self):
        return bytes((MessageKind.DISCONNECT,))


@dataclass(frozen=True)
class TextMessage(Message):
    nickname: str
    text: str

    def __bytes__(self):
        return (
            bytes((MessageKind.TEXT,))
            + self.nickname.encode("ascii")
            + b":"
            + self.text.encode("ascii")
        )


class Room(ABC):
    """
    A chat room.

    The on_* functions are called in response to various events in the room
    when manage() executes.

    To attach a room participant locally, call connect_client(). The returned
    LocalClient will announce itself like a real calculator and can be used
    to send messages into the room.
    """

    endpoint: Endpoint
    calculators: dict[Address, tuple["Calculator", Task]]

    def __init__(self, endpoint: Endpoint):
        self.endpoint = endpoint
        self.calculators = {}

    async def remove_calculator(self, calc: "Calculator"):
        (calc, _task) = self.calculators.pop(calc.address)
        await self.on_client_disconnect(calc)

    @abstractmethod
    async def on_client_connect(self, calc: "Calculator"):
        pass  # pragma: nocover

    @abstractmethod
    async def on_client_rename(self, calc: "Calculator", old_name: str):
        pass  # pragma: nocover

    @abstractmethod
    async def on_client_disconnect(self, calc: "Calculator"):
        pass  # pragma: nocover

    @abstractmethod
    async def on_message(self, sender: "Calculator", message: TextMessage):
        pass  # pragma: nocover

    async def _handle_client(self, calc: "Calculator"):
        try:
            await calc.process()
        finally:
            await self.remove_calculator(calc)

    async def _process_frame(self):
        (src, dst, payload) = await self.endpoint.receive_frame()
        contents = payload[1:].decode("ascii", errors="replace")

        (calc, _task) = self.calculators.get(src, (None, None))

        match payload[0]:
            case MessageKind.PING:
                message = PingMessage(src, dst, nickname=contents.rstrip("\0"))
                if calc is not None and calc.nickname != message.nickname:
                    old_nickname = calc.nickname
                    calc.nickname = message.nickname
                    await self.on_client_rename(calc, old_nickname)
            case MessageKind.DISCONNECT:
                message = DisconnectMessage(src, dst)
            case MessageKind.TEXT:
                (nickname, _, text) = contents.partition(":")
                message = TextMessage(src, dst, nickname=nickname, text=text)
            case _other:
                logger.debug("Ignoring unknown message with type %d", payload[0])
                return

        if src not in self.calculators:
            await self._add_calculator(src, message.nickname)

        if calc is not None:
            # Update timeout for the calculator
            await calc.rx.put(message)
            # Run room callback for new messages
            if isinstance(message, TextMessage):
                await self.on_message(calc, message)

    async def _add_calculator(self, addr: Address, nickname: str):
        calc = Calculator(addr, self, nickname, Queue(8))

        task = create_task(self._handle_client(calc), name=f"Calculator {addr}")
        self.calculators[addr] = (calc, task)
        await self.on_client_connect(calc)
        return (calc, task)

    async def manage(self):
        """
        Handle received messages, maintaining the set of connected clients
        and calling the on_* functions in response to room events.

        This coroutine terminates only if an error occurs.
        """
        try:
            while True:
                await self._process_frame()
        finally:
            # Capture the tasks to avoid iterator invalidation on dict values
            # when the tasks clean themselves up.
            tasks = tuple(task for _, task in self.calculators.values())

            for task in tasks:
                task.cancel()
                try:
                    await task
                except CancelledError:
                    pass

            self.calculators.clear()

    async def send_message(self, message: Message):
        await self.endpoint.send_frame(
            src=message.source, dest=message.destination, payload=bytes(message)
        )

    def connect_client(self, nickname: str, address: Address) -> "LocalClient":
        """
        Return a local client connected to the room that appears as a regular
        client to other clients.

        Local clients are useful for sending messages into a room.
        """
        return LocalClient(self, address, nickname)


@dataclass
class Calculator:
    address: Address
    room: Room
    nickname: str
    rx: Queue

    async def process(self):
        while True:
            # Calculators normally broadcast their presence every ~5
            # seconds; on timeout we automatically remove them.
            async with asyncio.timeout(60):
                msg = await self.rx.get()
                self.rx.task_done()
                if isinstance(msg, DisconnectMessage):
                    break


@dataclass
class LocalClient:
    room: Room
    address: Address
    nickname: str
    ping_task: Optional[Task] = None

    async def _send_ping(self):
        await self.room.send_message(
            PingMessage(
                source=self.address,
                destination=Address.BROADCAST,
                nickname=self.nickname,
            )
        )

    async def _announce_presence(self):
        while True:
            await asyncio.sleep(5)
            await self._send_ping()

    async def __aenter__(self):
        self.ping_task = asyncio.create_task(
            self._announce_presence(), name=f"{self} _announce_presence"
        )
        await self._send_ping()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        self.ping_task.cancel()
        await self.room.send_message(
            DisconnectMessage(source=self.address, destination=Address.BROADCAST)
        )

    async def send_message(self, text: str):
        await self.room.send_message(
            TextMessage(
                source=self.address,
                destination=Address.BROADCAST,
                nickname=self.nickname,
                text=text,
            )
        )
