import random
from dataclasses import dataclass
from typing import ClassVar


@dataclass(frozen=True)
class Address:
    value: bytes
    BROADCAST: ClassVar["Address"]
    GCN_BRIDGE: ClassVar["Address"]

    def __new__(cls, value):
        if len(value) != 5:
            raise ValueError(
                f"Address {value} must be exactly 5"
                f" bytes, but is {len(value)} bytes"
            )
        return super().__new__(cls)

    def __str__(self):
        return self.value.hex().upper()

    def __bytes__(self):
        return self.value

    @classmethod
    def random(cls):
        """
        Return a random address that is guaranteed not to conflict with the
        ID of any real calculator.
        """
        while True:
            first = random.randrange(256)
            if first not in (0x04, 0x0A):
                break

        return cls(first.to_bytes(1) + random.randbytes(4))


Address.BROADCAST = Address(bytes(5))
Address.GCN_BRIDGE = Address(b"\xaa\xaa\xaa\xaa\xaa")
