import asyncio
import logging
from typing import Optional

import usb.core

from . import Endpoint
from .. import Address

try:
    import libusb_package

    find_devices = libusb_package.find
except ImportError:
    find_devices = usb.core.find


logger = logging.getLogger(__name__)


class DirectUsbEndpoint(Endpoint):
    VID_TI = 0x0451
    PIDS = {
        0xE003: "TI-84+",
        0xE008: "TI-84+ SE",
    }

    EP_IN = 0x81
    EP_OUT = 0x02

    @classmethod
    def detect(cls):
        for dev in find_devices(
            idVendor=cls.VID_TI,
            custom_match=lambda d: d.idProduct in cls.PIDS,
            find_all=True,
        ):
            yield cls(dev)

    dev: usb.core.Device

    def __init__(self, dev: usb.core.Device):
        self.dev = dev

    async def __aenter__(self):
        await asyncio.to_thread(self.dev.set_configuration)
        return self

    @property
    def usb_address(self) -> tuple[int, int]:
        return self.dev.bus, self.dev.address

    def __str__(self):
        (bus, addr) = self.usb_address
        return f"{self.PIDS[self.dev.idProduct]} at USB bus {bus}, address {addr}"

    def ping(self):
        usb.control.get_status(self.dev)

    async def send_frame(self, src: Address, dest: Address, payload: bytes):
        frame = self.build_frame(src, dest, payload)
        logger.debug(
            "Sending frame to USB (src=%s, dest=%s): %s",
            src,
            dest,
            frame.hex(sep=" "),
        )
        await asyncio.to_thread(
            self.dev.write,
            self.EP_OUT,
            frame,
            timeout=0,
        )

    async def try_receive_frame(self) -> Optional[tuple[Address, Address, bytes]]:
        try:
            # Poll with timeout so this can be cancelled when run as a coroutine.
            data = (
                await asyncio.to_thread(
                    self.dev.read,
                    self.EP_IN,
                    # TODO: can probably reduce this to the maximum allowed length of a
                    # frame (including preamble etc).
                    500,
                    timeout=100,
                )
            ).tobytes()
        except usb.core.USBTimeoutError:
            return None

        logger.debug("Received frame from USB: %s", data.hex(sep=" "))
        return self.decode_frame(data)
