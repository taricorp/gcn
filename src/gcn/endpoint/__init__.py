import asyncio
import logging
from abc import ABC, abstractmethod
from typing import Optional, Collection, Callable

from gcn import Address


logger = logging.getLogger(__name__)


class InvalidFrame(Exception):
    pass


class Endpoint(ABC):
    """
    An endpoint is a connection to a CALCnet network, either physical (such
    as through USB to a real calculator) or virtual (like to a virtual CALCnet
    hub through a gCn metahub over the Internet).

    An endpoint should be used as an async context manager; entering the block
    prepares any required resources (like connecting to a server), and leaving
    it cleans them up.

    >>> async with Client('localhost', 4295, Address.GCN_BRIDGE) as client:
    ...     await client.join_hub('ChatHub')
    ...     await client.send_frame(Address.BROADCAST, b'Hi')
    ...     await asyncio.sleep(1)
    """

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        pass

    @staticmethod
    def build_frame(src: Address, dest: Address, payload: bytes) -> bytes:
        """
        Compose a CALCnet frame, without the preamble (\xff\x89) or postamble
        (\x2a) of the CALCnet wire protocol.
        """
        return (
            # Destination, source address
            bytes(dest)
            + bytes(src)
            # Size of message
            + len(payload).to_bytes(2, byteorder="little")
            # Message data
            + payload
        )

    @staticmethod
    def decode_frame(data: bytes) -> tuple[Address, Address, bytes]:
        """
        Decode a raw CALCnet frame (without the preamble or postamble) into its
        components.

        Returns a tuple of (source_address, dest_address, payload) or raises
        InvalidFrame if the frame is malformed.
        """
        dst = Address(data[:5])
        src = Address(data[5:10])
        dlen = int.from_bytes(data[10:12], byteorder="little")
        payload = data[12:]
        if len(payload) != dlen:
            raise InvalidFrame(
                f"Received frame with actual payload length {len(payload)}, but reported {dlen}"
            )

        return src, dst, payload

    @abstractmethod
    async def send_frame(self, src: Address, dest: Address, payload: bytes):
        """
        Send a calcnet frame.

        This constructs a calcnet frame from this client's calcnet address,
        destined for the provided destination address containing the provided
        bytes and then sends it to the attached network for delivery to the
        appropriate client(s).

        To send a broadcast frame, set the destination address to
        Address.BROADCAST.
        """
        raise NotImplementedError("Subclasses should implement send_frame")

    @abstractmethod
    async def try_receive_frame(self) -> Optional[tuple[Address, Address, bytes]]:
        """
        Wait for a CALCnet frame to arrive, the same as receive_frame(). Return
        None if none is currently available, or raise InvalidFrame if malformed.

        Implementations of this function should not return immediately, but may
        limit the polling time; users will use receive_frame() which hides polling.
        """
        raise NotImplementedError("Subclasses should implement try_receive_frame")

    async def receive_frame(self) -> tuple[Address, Address, bytes]:
        """
        Wait for a calcnet frame to arrive, returning a tuple of
        (source_addr, dest_addr, payload).

        Malformed frames are ignored and will be silently discarded (continuing
        to wait for a well-formed frame to arrive).
        """
        f = None
        while f is None:
            try:
                f = await self.try_receive_frame()
            except InvalidFrame as e:
                logger.info("Ignoring malformed frame received from %s: %e", self, e)
        return f


async def bridge(
    endpoints: Collection[Endpoint],
    frame_callback: Optional[
        Callable[[Endpoint, Address, Address, bytes], None]
    ] = None,
):
    """
    Bridge all of the provided endpoints together, receiving frames from each
    and sending received frames to each of the other endpoints.

    If frame_callback is provided, it will be called for each frame that is
    received, with the endpoint the frame was received on and the frame
    contents (source address, destination address, and payload).

    This coroutine will only terminate with an exception, raising an
    ExceptionGroup containing any errors that occurred that caused termination.
    """

    async def ep_rx(ep: Endpoint):
        while True:
            # Receive a frame
            (src, dst, payload) = await ep.receive_frame()
            if frame_callback is not None:
                frame_callback(ep, src, dst, payload)

            # Retransmit frame to all other endpoints
            async with asyncio.TaskGroup() as tg:
                for other_ep in endpoints:
                    if other_ep is ep:
                        continue
                    tg.create_task(other_ep.send_frame(src, dst, payload))

    async with asyncio.TaskGroup() as tg:
        for ep in endpoints:
            tg.create_task(ep_rx(ep), name=f"ep_rx for {ep}")
