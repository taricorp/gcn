import asyncio
import logging
from asyncio import StreamReader, StreamWriter
from typing import Optional

from gcn import Address
from gcn.endpoint import Endpoint, InvalidFrame

logger = logging.getLogger(__name__)


class InternetEndpoint(Endpoint):
    """
    An endpoint that communicates with a gCn metahub over the Internet.
    """

    connect_host: str
    connect_port: int
    virtual_hub: bytes
    name: bytes
    connect_kwargs: Optional[dict]
    local_addresses: set[Address]
    _r: StreamReader
    _w: StreamWriter

    def __init__(
        self,
        connect_host: str,
        connect_port: int,
        virtual_hub: str,
        name: str,
        local_addresses: set[Address] = None,
        connect_kwargs=None,
    ):
        """
        Create a new client that connects to a chosen gCn metahub and virtual
        hub.

        name is passed to the metahub on connect and may be publicly visible,
        but is not otherwise used in the protocol.

        connect_host, connect_port and connect_kwargs are passed to
        asyncio.open_connection to open the hub connection. For unencrypted
        connections no options beyond the host and port are usually required,
        but SSL/TLS connections will generally require additional options
        (possibly just ssl=True).
        """
        virtual_hub = virtual_hub.encode("ascii")
        if len(virtual_hub) > 16 or not virtual_hub:
            raise ValueError("Virtual hub name must be 1-16 characters long")
        name = name.encode("ascii")
        if len(name) > 16 or not name:
            raise ValueError("Client name must be 1-16 characters long")

        self.connect_host = connect_host
        self.connect_port = connect_port
        self.connect_kwargs = connect_kwargs
        self.virtual_hub = virtual_hub
        self.name = name
        self.local_addresses = local_addresses or set()
        self._r = self._w = None

    async def __aenter__(self):
        (r, w) = await asyncio.open_connection(
            self.connect_host, self.connect_port, **(self.connect_kwargs or {})
        )
        self._r = r
        self._w = w

        await self._join_hub()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        self._w.close()
        await self._w.wait_closed()
        self._r = self._w = None

    async def _send_message(self, msgtype: bytes, body: bytes):
        if len(msgtype) != 1:
            raise ValueError("Message types must be 1 byte")

        self._w.write(len(body).to_bytes(2, byteorder="little") + msgtype + body)
        await self._w.drain()

    async def _join_hub(self):
        """
        Connect this client to the configured virtual hub.
        """
        await self._send_message(
            b"j",
            len(self.virtual_hub).to_bytes(1)
            + self.virtual_hub
            + len(self.name).to_bytes(1)
            + self.name,
        )

        for addr in self.local_addresses:
            await self._add_local_address(addr)

    async def _add_local_address(self, address: Address):
        self.local_addresses.add(address)
        await self._send_message(b"c", bytes(address).hex().upper().encode("ascii"))

    async def send_frame(self, src: Address, dest: Address, payload: bytes):
        if not src in self.local_addresses:
            await self._add_local_address(src)

        frame = (
            # Magic bytes for some reason
            b"\xff\x89"
            + self.build_frame(src, dest, payload)
            # Another magic byte for some reason
            + b"\x2a"
        )
        logger.debug("Sending frame of size %d: %s", len(payload), frame.hex(sep=" "))
        await self._send_message(b"b" if dest == Address.BROADCAST else b"f", frame)

    async def _receive_message(self) -> Optional[bytes]:
        size = int.from_bytes(await self._r.readexactly(2), byteorder="little")
        ty = await self._r.readexactly(1)
        message = await self._r.readexactly(size)

        logger.debug(
            "Received frame of size %d with type %s: %s", size, ty, message.hex(sep=" ")
        )
        if ty not in (b"f", b"b"):
            raise InvalidFrame(f"Message type {ty} is not recognized")

        return message

    async def try_receive_frame(self) -> Optional[tuple[Address, Address, bytes]]:
        message = await self._receive_message()
        if message is None:
            return None

        magic1 = message[:2]
        magic2 = message[-1]
        if magic1 != b"\xff\x89":
            raise InvalidFrame(f"Frame preamble missing or corrupt: {magic1}")
        if magic2 != 42:
            raise InvalidFrame(f"Frame postamble missing or corrupt: {magic2}")

        return self.decode_frame(message[2:-1])
