import argparse
import asyncio
import logging
import sys
from typing import Collection

import usb.control
import usb.core
import usb.core

import gcn
from gcn import InternetEndpoint, DirectUsbEndpoint
from gcn.endpoint import bridge
from gcn.tools.options import add_gcn_internet_options, create_internet_endpoint

try:
    import libusb_package

    find_devices = libusb_package.find
except ImportError:
    find_devices = usb.core.find


logger = logging.getLogger(__name__)


async def run(args):
    async with (
        find_calculator(args) as usb,
        create_internet_endpoint(args) as gcn,
    ):
        print("Connected to calculator:", usb)
        print("Joined virtual hub:", gcn.virtual_hub.decode())
        await bridge((gcn, usb))


class NoCalculators(Exception):
    pass


class MultipleCalculators(Exception):
    calculators: Collection[DirectUsbEndpoint]

    def __init__(self, calculators: Collection[DirectUsbEndpoint]):
        super().__init__()
        self.calculators = calculators


def find_calculator(args) -> DirectUsbEndpoint:
    calcs = [
        c
        for c in DirectUsbEndpoint.detect()
        if (args.usb_bus is None or c.usb_address[0] == args.usb_bus)
        and (args.usb_address is None or c.usb_address[1] == args.usb_address)
    ]
    if not calcs:
        raise NoCalculators()
    if len(calcs) > 1:
        raise MultipleCalculators(calcs)

    [calc] = calcs
    return calc


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--version", "-V", action="version", version=gcn.__version__)
    parser.add_argument(
        "--log-level",
        default="WARNING",
        choices=["DEBUG", "INFO", "WARNING", "ERROR"],
    )
    add_gcn_internet_options(parser)

    group = parser.add_argument_group(
        "USB devices",
        description=(
            "Select a calculator to use, by USB address. Only needed if "
            "multiple calculators are available."
        ),
    )
    group.add_argument(
        "--usb-bus",
        type=int,
        metavar="BUS",
        help="Only consider calculators connected to this USB bus number",
    )
    group.add_argument(
        "--usb-address",
        type=int,
        metavar="ADDR",
        help="Only consider calculators with this USB address",
    )
    args = parser.parse_args()

    logging.basicConfig(level=args.log_level)

    try:
        asyncio.run(run(args), debug=True)
    except* ConnectionError as e:
        print(
            "error: metahub connection failed:",
            ", ".join(str(x) for x in e.exceptions),
            file=sys.stderr,
        )
    except* EOFError:
        print("error: metahub closed connection", file=sys.stderr)
    except* usb.core.USBError as e:
        print(
            "error: USB connection failed:",
            ", ".join(str(x) for x in e.exceptions),
            file=sys.stderr,
        )
    except* NoCalculators:
        print("error: no calculators found", file=sys.stderr)
        if args.usb_bus is not None or args.usb_address is not None:
            print(
                "hint: run without the --usb-bus or --usb-address option "
                "to search for any available calculator",
                file=sys.stderr,
            )
    except* MultipleCalculators as e:
        print("error: multiple calculators found:", file=sys.stderr)
        for calc in e.calculators:
            print(" *", calc, file=sys.stderr)
        print(
            "hint: specify --usb-bus and/or --usb-address to select one",
            file=sys.stderr,
        )
    else:
        return

    sys.exit(1)


if __name__ == "__main__":
    main()
