import argparse
import gcn
import platform


def add_gcn_internet_options(parser: argparse.ArgumentParser):
    group = parser.add_argument_group(
        "Required CALCnet options",
    )
    group.add_argument(
        "--virtual-hub",
        "-n",
        help="gCn virtual hub to attach to",
        required=True,
    )
    group.add_argument(
        "--name",
        "-l",
        help="Name of this client to be reported to the metahub (default: %(default)s)",
        default=platform.node(),
    )

    group = parser.add_argument_group(
        "Connection", description="How to connect to a gCn metahub"
    )
    group.add_argument(
        "--host",
        "-s",
        default="gcnhub.cemetech.net",
        help="Hostname or IP address to connect to (default: %(default)s)",
    )
    group.add_argument(
        "--port",
        "-p",
        type=int,
        default=4295,
        help="Port to connect to (default: %(default)s)",
    )
    group.add_argument(
        "--ssl",
        action="store_true",
        help="Use SSL/TLS for metahub connection (default: %(default)s)",
    )
    group.add_argument(
        "--ssl-port",
        type=int,
        default=4296,
        metavar="PORT",
        help=(
            "Port to connect to for SSL/TLS connections. "
            "Only used if --ssl is also specified (default: %(default)s)"
        ),
    )


def create_internet_endpoint(args: argparse.Namespace) -> gcn.InternetEndpoint:
    port = args.ssl_port if args.ssl else args.port

    return gcn.InternetEndpoint(
        args.host, port, args.virtual_hub, args.name, connect_kwargs=dict(ssl=args.ssl)
    )
