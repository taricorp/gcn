import argparse
import asyncio
import sys

from gcn import Address
from gcn.chat import TextMessage, Room as BaseRoom, Calculator
from gcn.tools.options import add_gcn_internet_options, create_internet_endpoint


async def asyncify_stdio():
    """
    Return a pair of (reader, writer) asyncio streams bound to standard input
    and standard output.

    Based on the implementation shared in https://stackoverflow.com/a/64317899.
    """
    loop = asyncio.get_event_loop()

    reader = asyncio.StreamReader()
    protocol = asyncio.StreamReaderProtocol(reader)
    await loop.connect_read_pipe(lambda: protocol, sys.stdin)

    w_transport, w_protocol = await loop.connect_write_pipe(
        asyncio.streams.FlowControlMixin, sys.stdout
    )
    writer = asyncio.StreamWriter(w_transport, w_protocol, reader, loop)

    return reader, writer


class Room(BaseRoom):
    console_in: asyncio.StreamReader
    console_out: asyncio.StreamWriter

    def __init__(self, endpoint, console_in, console_out):
        super().__init__(endpoint)
        self.console_in = console_in
        self.console_out = console_out

    async def on_client_connect(self, calc: Calculator):
        print(f"*** {calc.nickname} connected from {calc.address}")

    async def on_client_rename(self, calc: Calculator, old_name: str):
        print(f"*** {old_name} renamed to {calc.nickname}")

    async def on_client_disconnect(self, calc: Calculator):
        print(f"*** {calc} disconnected")

    async def on_message(self, sender: Calculator, message: TextMessage):
        print(f"<{message.nickname}> {message.text}")

    async def handle_console_input(self, local_client):
        while True:
            line = await self.console_in.readline()
            await local_client.send_message(line.decode().strip())

    async def manage(self, local_client):
        room_task = asyncio.create_task(super().manage(), name="Room management")
        console_task = asyncio.create_task(
            self.handle_console_input(local_client), name="Console"
        )
        try:
            await asyncio.wait(
                (room_task, console_task), return_when=asyncio.FIRST_COMPLETED
            )
        except KeyboardInterrupt:
            room_task.cancel()
            console_task.cancel()
            try:
                await room_task
                await console_task
            except asyncio.CancelledError:
                pass


async def run(args):
    try:
        stdin, stdout = await asyncify_stdio()
        async with create_internet_endpoint(args) as ep:
            print("Connected to", args.virtual_hub, "on", args.host, "as", args.name)
            room = Room(endpoint=ep, console_in=stdin, console_out=stdout)
            async with room.connect_client(
                ep.name.decode()[:8], Address.random()
            ) as local_client:
                await room.manage(local_client)
    except KeyboardInterrupt:
        pass


def main():
    parser = argparse.ArgumentParser(
        description="""\
Simple CALCnet chat client. Writes messages from standard input into a chat
room, prints messages and status updates to standard output."""
    )
    add_gcn_internet_options(parser)

    args = parser.parse_args()
    asyncio.run(run(args))


if __name__ == "__main__":
    sys.exit(main())
