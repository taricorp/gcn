#!/usr/bin/python
import argparse
import asyncio
import dataclasses
import logging
import os
import ssl
from dataclasses import dataclass
from io import BytesIO
from typing import Optional, Collection, Callable, Coroutine

import prometheus_client


CONNECTED_CALCULATORS = prometheus_client.Gauge(
    "gcnhub_connected_calculators", "Calculators connected to virtual hubs", ["hub"]
)
BYTES_RECEIVED = prometheus_client.Counter(
    "gcnhub_bytes_received", "Bytes of data received from clients"
)
BYTES_SENT = prometheus_client.Counter(
    "gcnhub_bytes_sent", "Bytes of data transmitted to clients"
)


class VirtualHub:
    """
    A virtual hub is a CalcNet network.

    All clients connected to a virtual hub receive all broadcasts to the
    hub, and any calculator connected to one of the hub's clients can receive
    unicast messages.
    """

    clients: set["Client"]
    name: str
    logger: logging.LoggerAdapter

    class LogAdapter(logging.LoggerAdapter):
        def __init__(self, logger, vhub):
            super().__init__(logger)
            self.vhub = vhub

        def process(self, msg, kwargs):
            return f"VirtualHub {self.vhub}: {msg}", kwargs

    def __init__(self, name: str):
        self.name = name
        self.clients = set()
        self.logger = self.LogAdapter(LOGGER, self)

    def __str__(self):
        return self.name

    def add(self, client: "Client"):
        """
        Add the specified client to this virtual hub.
        """
        CONNECTED_CALCULATORS.labels(hub=self.name).inc()
        self.clients.add(client)

    def remove(self, client: "Client"):
        """
        Remove the specified client from this virtual hub.
        """
        self.clients.remove(client)
        CONNECTED_CALCULATORS.labels(hub=self.name).dec()

    async def broadcast(self, message: bytes, sender: "Client"):
        """
        Send a copy of the provided message to every client.
        """
        # Start all the writes concurrently, then wait for completion
        # of each.
        writes = [c.write(message) for c in self.clients if c != sender]
        for write in asyncio.as_completed(writes):
            try:
                await write
            except Client.WriteError as e:
                # Client is dead
                client = e.client
                self.logger.info(
                    (
                        "Connection to client %s failed "
                        "while sending broadcast message"
                    ),
                    client,
                    exc_info=True,
                )
                self.remove(client)

    async def unicast(self, sid: str, message: bytes):
        """
        Send the provided message to the client that has the connected
        calculator identified by the given sid.
        """
        for client in self.clients:
            if sid in client.calculators:
                try:
                    await client.write(message)
                except Client.WriteError as e:
                    # Client is dead
                    client = e.client
                    self.logger.info(
                        (
                            "Connection to client %s failed "
                            "while sending unicast message"
                        ),
                        client,
                        exc_info=True,
                    )
                    self.remove(client)
                break
        else:
            self.logger.warning(
                "No client found for unicast message with destination %s",
                sid,
            )


LOGGER = logging.getLogger(__name__)


class InstrumentedStreamReader:
    def __init__(self, reader: asyncio.StreamReader):
        self._reader = reader

    async def _instrument(self, coro):
        buf = await coro
        BYTES_RECEIVED.inc(len(buf))
        return buf

    async def read(self, n=-1):
        return await self._instrument(self._reader.read(n))

    async def readexactly(self, n):
        return await self._instrument(self._reader.readexactly(n))

    async def readuntil(self, separator=b"\n"):
        return await self._instrument(self._reader.readuntil(separator))

    def __getattr__(self, name):
        return getattr(self._reader, name)


class InstrumentedStreamWriter(asyncio.StreamWriter):
    def __init__(self, writer: asyncio.StreamWriter):
        self._writer = writer

    def write(self, data):
        BYTES_SENT.inc(len(data))
        return self._writer.write(data)

    def writelines(self, data):
        BYTES_SENT.inc(sum(len(x) for x in data))
        return self._writer.writelines(data)

    def __getattr__(self, name):
        return getattr(self._writer, name)


class Client:
    """
    A connected gCn client.
    """

    hub: "GcnHub"
    "The hub that this client is connected to."
    name: Optional[str]
    "The name this client has specified for itself, if any."
    calculators: set[str]
    "The set of calculator IDs which are connected through this client."
    virtual_hub: Optional[VirtualHub]
    "The virtual hub that this client is connected to, if any."

    reader: asyncio.StreamReader
    writer: asyncio.StreamWriter
    logger: logging.LoggerAdapter

    MAX_FRAME_LEN = 256 + 5 + 5 + 2

    class LogAdapter(logging.LoggerAdapter):
        def __init__(self, logger, client):
            super().__init__(logger)
            self.client = client

        def process(self, msg, kwargs):
            msg = f"client {self.client}: " + msg
            return msg, kwargs

    def __init__(self, hub, logger, reader, writer):
        self.hub = hub
        self.name = None
        self.calculators = set()
        self.reader = reader
        self.writer = writer
        self.logger = self.LogAdapter(logger, self)
        self.virtual_hub = None

    def __str__(self):
        remote_addr = self.writer.get_extra_info("peername")
        return f'"{self.name}"@{remote_addr}'

    class WriteError(Exception):
        def __init__(self, client: "Client"):
            super().__init__()
            self.client = client

        def __str__(self):
            return str(self.__cause__)

    async def write(self, data: bytes):
        """
        Send the given data to this client.
        """
        self.writer.write(data)
        try:
            await self.writer.drain()
        except Exception as e:
            raise self.WriteError(self) from e

    async def run(self):
        while True:
            await self.handle_message()

    async def handle_message(self):
        message_len = int.from_bytes(
            await self.reader.readexactly(2), byteorder="little"
        )
        [message_type] = await self.reader.readexactly(1)

        if message_len > 300:
            raise ValueError(f"Message with length {message_len} is unacceptably large")
        message = BytesIO(await self.reader.readexactly(message_len))

        match chr(message_type):
            case "j":  # Join virtual hub
                await self.handle_join_message(message)
            case "c":  # Add calculator
                await self.handle_calculator_message(message)
            case "b":  # Broadcast frame
                await self.handle_broadcast_message(message)
            case "f":  # Directed frame
                await self.handle_unicast_message(message)
            case ty:
                self.logger.error(
                    f'Ignoring message of unknown type "{ty}" with length {message_len}'
                )
                return

        len_read = message.tell()
        len_unread = message.seek(0, os.SEEK_END) - len_read
        if len_unread > 0:
            self.logger.warning(f"Ignoring {len_unread} excess byte(s) in message")

    async def handle_join_message(self, message: BytesIO):
        """
        Handle a message requesting this client join a selected virtual hub.
        """
        # Read the name of the hub we want to join
        [hub_name_len] = message.read(1)
        if hub_name_len > 16:
            raise ValueError(f"Hub name with length {hub_name_len} is too large")
        hub_name = message.read(hub_name_len).decode("utf-8")

        # Read the name the client is using. Client names are not used in the
        # protocol; only for informational purposes.
        [client_name_len] = message.read(1)
        if client_name_len > 16:
            raise ValueError(f"Client name with length {client_name_len} is too large")
        client_name = message.read(client_name_len).decode("utf-8")
        if self.name != client_name:
            if self.name is not None:
                self.logger.info("Renaming client to %s", client_name)
            self.name = client_name

        # Connect the client to the desired hub
        if self.virtual_hub is not None:
            self.logger.warning(
                "Joining hub %s, but already connected to hub %s",
                hub_name,
                self.virtual_hub.name,
            )
            self.virtual_hub.remove(self)
            self.virtual_hub = None

        self.logger.info("Joining hub %s", hub_name)
        self.virtual_hub = self.hub.get_virtual_hub(hub_name)
        self.virtual_hub.add(self)

    async def handle_calculator_message(self, message: BytesIO):
        """
        Handle a message associating a calculator with this client.
        """
        # Calculator IDs are bytes, but in this message it's preformatted
        # as a hexadecimal string.
        sid = message.read().decode("ascii")
        if len(sid) != 10:
            raise ValueError(
                f"Calculator ID {sid} must be exactly 10 "
                f"characters, but is {len(sid)} characters"
            )
        self.calculators.add(sid)

    @staticmethod
    def pack_message(ty: str, data: bytes) -> bytes:
        assert len(ty) == 1
        return (
            len(data).to_bytes(length=2, byteorder="little") + ty.encode("ascii") + data
        )

    async def handle_broadcast_message(self, message: BytesIO):
        """
        Handle a messsage containing data to broadcast to all clients on
        the same virtual hub.
        """
        if self.virtual_hub is None:
            self.logger.warning(
                "Ignoring broadcast because client is not connected to a virtual hub"
            )
            return

        payload = message.read()
        if len(payload) > self.MAX_FRAME_LEN:
            raise ValueError(
                "Broadcast message with length {len(payload)} is too large"
            )

        self.logger.debug("Broadcasting message with %d-byte payload", len(payload))
        await self.virtual_hub.broadcast(self.pack_message("b", payload), self)

    async def handle_unicast_message(self, message: BytesIO):
        message = message.read()
        if len(message) > self.MAX_FRAME_LEN:
            raise ValueError(f"Unicast message with length {len(message)} is too large")

        # Pull out the destination calculator ID to route this frame
        # to the correct client
        dest_sid = "".join(f"{b:02X}" for b in message[2:7])
        if dest_sid in self.calculators:
            self.logger.warning(
                "Ignoring unicast frame for calculator {dest_sid} "
                "which is connected to this client"
            )
            return

        self.logger.debug(
            "Unicast message to %s with length %d", dest_sid, len(message)
        )
        await self.virtual_hub.unicast(dest_sid, self.pack_message("f", message))

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        if self.virtual_hub is not None:
            self.virtual_hub.remove(self)
            self.virtual_hub = None

        self.writer.close()
        await self.writer.wait_closed()

    @classmethod
    async def handle_connection(
        cls, hub: "GcnHub", reader: asyncio.StreamReader, writer: asyncio.StreamWriter
    ):
        try:
            async with cls(
                hub,
                LOGGER,
                InstrumentedStreamReader(reader),
                InstrumentedStreamWriter(writer),
            ) as client:
                await client.run()
        except Exception as e:
            LOGGER.debug("client disconnected: %s", e)


@dataclass
class ServerConfig:
    host: str = "0.0.0.0"
    port: int = 4295
    tls: Optional[ssl.SSLContext] = None
    extra_server_args: dict = dataclasses.field(default_factory=dict)


class GcnHub:
    virtual_hubs: dict[str, VirtualHub]

    def __init__(self, configs: Collection[ServerConfig]):
        self.virtual_hubs = {}
        self.configs = configs

    async def run_async(
        self,
        server_callback: Optional[
            Callable[[asyncio.Server], Optional[Coroutine]]
        ] = None,
    ):
        async with asyncio.TaskGroup() as tg:
            for config in self.configs:
                server = await asyncio.start_server(
                    lambda r, w: Client.handle_connection(self, r, w),
                    host=config.host,
                    port=config.port,
                    ssl=config.tls,
                    **config.extra_server_args,
                )
                LOGGER.info(
                    "Listening on %s:%s with TLS %s",
                    config.host,
                    config.port,
                    config.tls,
                )

                if server_callback is not None:
                    cb_handler = server_callback(server)
                    if cb_handler is not None:
                        tg.create_task(cb_handler)

                tg.create_task(server.serve_forever())

    def run(self):
        asyncio.run(self.run_async())

    def get_virtual_hub(self, hub_name: str) -> VirtualHub:
        if hub_name in self.virtual_hubs:
            return self.virtual_hubs[hub_name]
        else:
            h = VirtualHub(hub_name)
            self.virtual_hubs[hub_name] = h
            return h


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--log-level",
        default="INFO",
        choices=(
            "DEBUG",
            "INFO",
            "WARNING",
            "ERROR",
            "CRITICAL",
        ),
    )
    parser.add_argument(
        "--metrics-port",
        type=int,
        help=(
            "Port to listen for HTTP requests to serve runtime metrics. "
            "Default: do not serve metrics"
        ),
    )

    group = parser.add_argument_group(
        "networking",
        "Options controlling where %(prog)s listens for client connections.",
    )
    group.add_argument(
        "-H",
        "--listen-host",
        default="localhost",
        help="Bind to this address when listening for connections. Default: %(default)s",
    )
    group.add_argument(
        "-p",
        "--listen-port",
        type=int,
        default=[4295],
        action="append",
        help=(
            "Bind to this port listening for unencrypted connections. "
            "May be specified multiple times for multiple ports. "
            "Default: %(default)s"
        ),
    )
    group.add_argument(
        "-P",
        "--ssl-listen-port",
        type=int,
        default=[4296],
        action="append",
        help=(
            "Bind to this port listening for encrypted (SSL) connections. "
            "May be specified multiple times for multiple ports. "
            "Default: %(default)s"
        ),
    )

    group = parser.add_argument_group(
        "ssl", "Options specifying how to serve encrypted (SSL) connections."
    )
    group.add_argument(
        "-c",
        "--ssl-certfile",
        default=None,
        help="File from which to load a server certificate",
    )
    group.add_argument(
        "-k",
        "--ssl-keyfile",
        default=None,
        help=(
            "File from which to load a server private key. "
            "If not specified, the certificate file must also contain a private key."
        ),
    )
    group.add_argument(
        "-K",
        "--ssl-key-password",
        default=None,
        help=(
            "Password needed to decrypt the server private key. "
            "If required but not specified, %(prog)s will "
            "prompt for the password."
        ),
    )
    group.add_argument(
        "--ssl-only",
        action="store_true",
        help=(
            "Only listen with SSL: ignore --listen-port "
            "and only provide encrypted connections"
        ),
    )
    args = parser.parse_args()

    logging.basicConfig(
        format="%(asctime)s [%(name)s %(levelname)s] %(message)s", level=args.log_level
    )

    ctx = None
    if args.ssl_certfile:
        ctx = ssl.SSLContext()
        ctx.load_cert_chain(
            args.ssl_certfile, keyfile=args.ssl_keyfile, password=args.ssl_key_password
        )

    configs = []
    if not args.ssl_only:
        configs.extend(
            ServerConfig(host=args.listen_host, port=port, tls=None)
            for port in args.listen_port
        )

    if ctx is not None:
        configs.extend(
            ServerConfig(host=args.listen_host, port=port, tls=ctx)
            for port in args.ssl_listen_port
        )
    else:
        LOGGER.warning("No SSL certificate specified; will not serve SSL")

    if args.metrics_port is not None:
        prometheus_client.disable_created_metrics()
        prometheus_client.start_http_server(args.metrics_port)

    hub = GcnHub(configs)
    hub.run()


if __name__ == "__main__":
    main()
