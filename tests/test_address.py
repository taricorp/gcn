import unittest

from gcn.address import Address


class AddressTest(unittest.TestCase):
    def test_instance(self):
        addr = Address(b"ABCDE")
        self.assertEqual(bytes(addr), b"ABCDE")
        self.assertEqual(str(addr), "4142434445")

    def test_checks_value(self):
        with self.assertRaises(ValueError) as cm:
            Address(b"111")

        self.assertEqual(
            cm.exception.args,
            ("Address b'111' must be exactly 5 bytes, but is 3 bytes",),
        )

    def test_equality(self):
        self.assertEqual(Address.BROADCAST, Address(bytes(5)))
        self.assertNotEqual(Address.BROADCAST, Address.GCN_BRIDGE)
