import asyncio
from io import BytesIO
from unittest import mock, IsolatedAsyncioTestCase
from unittest.mock import AsyncMock, Mock, call

from gcn import InternetEndpoint, Address


class InternetEndpointTest(IsolatedAsyncioTestCase):
    async def wrap_readexactly(self, n):
        b = self.read_buf.read(n)
        if len(b) < n:
            raise asyncio.IncompleteReadError(expected=n, partial=b)
        return b

    def setUp(self):
        self.read_buf = BytesIO()
        self.client = InternetEndpoint(
            "localhost", 4321, "TestHub", "TestClient", {Address.GCN_BRIDGE}
        )
        self.client._r = Mock(readexactly=AsyncMock(side_effect=self.wrap_readexactly))
        self.client._w = Mock(drain=AsyncMock())

    def setReadData(self, data: bytes):
        self.read_buf = BytesIO(data)

    @mock.patch("asyncio.open_connection")
    async def test_basic_connection(self, mock_open_connection):
        reader = Mock()
        writer = Mock(wait_closed=AsyncMock(), drain=AsyncMock())
        mock_open_connection.return_value = (reader, writer)

        async with InternetEndpoint(
            "localhost",
            4321,
            "HUB",
            "Me",
            connect_kwargs={"ssl": True},
            local_addresses={Address.GCN_BRIDGE},
        ):
            pass

        mock_open_connection.assert_called_once_with("localhost", 4321, ssl=True)
        # Sent a join and new calculator message (because we prefilled
        # the local address).
        writer.write.assert_has_calls(
            [
                call(b"\x07\x00j\x03HUB\x02Me"),
                call(b"\x0a\x00cAAAAAAAAAA"),
            ]
        )
        writer.close.assert_called_once_with()
        writer.wait_closed.assert_awaited_once_with()

    async def test_send_frame(self):
        # Broadcast frame is sent with broadcast message type
        other_addr = Address.random()
        await self.client.send_frame(other_addr, Address.BROADCAST, b"BROADTEST")
        # Source address is added to the local addresses
        self.assertIn(other_addr, self.client.local_addresses)
        # New address was sent to the server, and the frame was sent
        self.client._w.write.assert_has_calls(
            [
                call(b"\x0a\x00c" + bytes(other_addr).hex().upper().encode("ascii")),
                call(
                    b"\x18\x00b"
                    + b"\xff\x89"
                    + bytes(Address.BROADCAST)
                    + bytes(other_addr)
                    + b"\x09\x00BROADTEST"
                    + b"\x2a"
                ),
            ]
        )

        # Unicast uses the unicast message type
        self.client._w.write.reset_mock()
        await self.client.send_frame(
            Address.GCN_BRIDGE, Address(b"\x12\x34\x56\x78\x90"), b"UNITEST"
        )
        # Client already had the bridge address, so doesn't send a new calculator message
        self.client._w.write.assert_called_once_with(
            b"\x16\x00f"
            + b"\xff\x89"
            + b"\x12\x34\x56\x78\x90"
            + bytes(Address.GCN_BRIDGE)
            + b"\x07\x00UNITEST"
            + b"\x2a"
        )

    async def test_receive_frame(self):
        self.setReadData(
            b"\x1d\x00f"
            + b"\xff\x89"
            + bytes(Address.GCN_BRIDGE)
            + b"\x01\x23\x45\x67\x89"
            + b"\x0e\x00GET / HTTP/1.1"
            + b"\x2a"
        )

        (src, dst, payload) = await self.client.receive_frame()
        self.assertEqual(src, Address(bytes((0x01, 0x23, 0x45, 0x67, 0x89))))
        self.assertEqual(dst, Address.GCN_BRIDGE)
        self.assertEqual(payload, b"GET / HTTP/1.1")

        self.assertEqual(
            b"",
            self.read_buf.read(),
            "receive_frame() should have consumed the entire message",
        )
