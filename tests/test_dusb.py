import array
from unittest import IsolatedAsyncioTestCase, mock
from unittest.mock import Mock

import usb.core

from gcn import DirectUsbEndpoint, Address


class DirectUsbEndpointTests(IsolatedAsyncioTestCase):
    def test_detect_devices(self):
        usb_devices = [
            Mock(name="MockDevice1", idProduct=0xE003),
            Mock(name="MockDevice2", idProduct=0x0420),
            Mock(name="MockDevice3", idProduct=0xE008),
        ]
        with mock.patch("gcn.endpoint.dusb.find_devices") as mock_find_devices:
            mock_find_devices.side_effect = lambda custom_match, **_: [
                d for d in usb_devices if custom_match(d)
            ]
            detected = list(DirectUsbEndpoint.detect())

        # It's looking for TI devices
        mock_find_devices.assert_called_once_with(
            idVendor=0x0451,
            custom_match=mock.ANY,
            find_all=True,
        )

        # Returned mock devices 1 and 3
        self.assertEqual([d.dev for d in detected], [usb_devices[0], usb_devices[2]])

    async def test_context_manager(self):
        mock_device = Mock()
        async with DirectUsbEndpoint(mock_device) as ep:
            pass

        # Sets the default device configuration
        mock_device.set_configuration.assert_called_once_with()

    async def test_str(self):
        mock_device = Mock(
            idProduct=0xE003,
            bus=1,
            address=80,
        )
        self.assertEqual(
            str(DirectUsbEndpoint(mock_device)), "TI-84+ at USB bus 1, address 80"
        )

    async def test_send_frame(self):
        mock_device = Mock()
        addr = Address.random()
        await DirectUsbEndpoint(mock_device).send_frame(
            addr, Address.GCN_BRIDGE, b"ZOOP"
        )

        mock_device.write.assert_called_once_with(
            0x02,
            bytes(Address.GCN_BRIDGE) + bytes(addr) + b"\x04\x00ZOOP",
            timeout=0,
        )

    async def test_receive_frame(self):
        mock_device = Mock()
        mock_device.read.side_effect = [
            usb.core.USBTimeoutError("Nothing yet!"),
            array.array(
                "B",
                b"\0\0\0\0\0\xaa\xaa\xaa\xaa\xaa\x04\x00POOZ",
            ),
        ]
        (src, dst, payload) = await DirectUsbEndpoint(mock_device).receive_frame()

        self.assertEqual(src, Address.GCN_BRIDGE)
        self.assertEqual(dst, Address(bytes(5)))
        self.assertEqual(payload, b"POOZ")
