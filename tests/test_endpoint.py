import asyncio
from unittest import IsolatedAsyncioTestCase
from unittest.mock import Mock

from gcn import Address
from gcn.endpoint import Endpoint, bridge


class BridgeTestCase(IsolatedAsyncioTestCase):
    async def test_bridge(self):
        ep1 = Mock(spec=Endpoint, name="ep1")
        ep1_addr = Address.random()
        ep2 = Mock(spec=Endpoint, name="ep2")
        ep2_addr = Address.random()
        # Each endpoint will provide a packet
        ep1.receive_frame.return_value = (ep1_addr, ep2_addr, b"Hey")
        ep2.receive_frame.return_value = (ep2_addr, ep1_addr, b"Hi")

        # Run bridge() and wait for bridging to occur
        bridge_task = asyncio.create_task(bridge((ep1, ep2)))
        async with asyncio.timeout(1):
            while ep1.send_frame.call_count == 0 or ep2.send_frame.call_count == 0:
                await asyncio.sleep(0)

        # Clean up bridge()
        with self.assertRaises(asyncio.CancelledError):
            bridge_task.cancel()
            await bridge_task

        # Received packets were forwarded to the other endpoint
        ep1.send_frame.assert_awaited_once_with(ep2_addr, ep1_addr, b"Hi")
        ep2.send_frame.assert_awaited_once_with(ep1_addr, ep2_addr, b"Hey")
