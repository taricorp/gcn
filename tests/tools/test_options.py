import argparse
import unittest
from unittest.mock import patch

import gcn.tools.options


class IpOptionsTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.parser = argparse.ArgumentParser()
        gcn.tools.options.add_gcn_internet_options(cls.parser)

    def test_with_ssl(self):
        args = self.parser.parse_args(
            [
                "--name",
                "TestClient",
                "--virtual-hub",
                "MyHub",
                "--host",
                "localhost",
                "--ssl",
            ]
        )

        with patch("gcn.InternetEndpoint", autospec=True) as mock_endpoint:
            ep = gcn.tools.options.create_internet_endpoint(args)

        mock_endpoint.assert_called_once_with(
            "localhost",
            4296,
            "MyHub",
            "TestClient",
            connect_kwargs={"ssl": True},
        )
        self.assertIs(ep, mock_endpoint.return_value)
