import asyncio
from asyncio import timeout
from unittest import IsolatedAsyncioTestCase, TestCase
from unittest.mock import AsyncMock, Mock, patch, call, DEFAULT

from gcn import Address
from gcn.chat import (
    Room,
    TextMessage,
    PingMessage,
    DisconnectMessage,
    LocalClient,
)
from gcn.endpoint import Endpoint


class MockfulRoom(Room):
    # These methods are abstract so must be defined on the class, but we want
    # them to be mock instances that get setup in __init__().
    on_client_connect = None
    on_client_rename = None
    on_client_disconnect = None
    on_message = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.on_client_connect = AsyncMock()
        self.on_client_rename = AsyncMock()
        self.on_client_disconnect = AsyncMock()
        self.on_message = AsyncMock()


class ChatTests(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        self.endpoint = Mock(spec=Endpoint)
        self.room = MockfulRoom(self.endpoint)

    async def asyncTearDown(self):
        # When a calculator task terminates it removes itself from
        # calculators, so we need to capture the tasks before cancelling
        # any: otherwise calculators changes while we're iterating over it.
        tasks = [t for (_, t) in self.room.calculators.values()]
        for task in tasks:
            task.cancel()
            try:
                await task
            except asyncio.CancelledError:
                pass

    async def test_connect_calculator(self):
        self.endpoint.receive_frame.return_value = (
            Address.GCN_BRIDGE,
            Address.BROADCAST,
            b"\xabCarl\0\0\0\0",
        )
        await self.room._process_frame()

        (calc, task) = self.room.calculators[Address.GCN_BRIDGE]
        self.assertEqual(calc.address, Address.GCN_BRIDGE)
        self.assertEqual(calc.nickname, "Carl")
        self.assertIs(calc.room, self.room)

    async def test_disconnect_calculator(self):
        (calc, task) = await self.room._add_calculator(Address.GCN_BRIDGE, "TestClient")
        self.assertIn(Address.GCN_BRIDGE, self.room.calculators)

        self.endpoint.receive_frame.return_value = (
            Address.GCN_BRIDGE,
            Address.BROADCAST,
            b"\xac",
        )
        await self.room._process_frame()

        # Once everything is processed, the calculator task terminates cleanly
        # and the calculator is removed from the room.
        async with asyncio.timeout(1):
            await task

        # The calculator was removed
        self.assertNotIn(Address.GCN_BRIDGE, self.room.calculators)
        # The room disconnect callback is also called.
        self.room.on_client_disconnect.assert_awaited_once_with(calc)

    # We need to patch this at testcase level rather than inside the test
    # because the background task won't be affected if we use a patch()
    # context manager instead.
    @patch("asyncio.timeout", autospec=True, name="mock_asyncio_timeout")
    async def test_removes_calculator_on_timeout(self, mock_timeout):
        (calc, task) = await self.room._add_calculator(Address.GCN_BRIDGE, "TestClient")

        # Wait for calculator task to start waiting for its timeout
        async with timeout(1):
            while len(mock_timeout.mock_calls) < 2:
                await asyncio.sleep(0)

        # It used a 60-second timeout as a context manager
        self.assertEqual(
            mock_timeout.mock_calls,
            [
                call(60),
                call().__aenter__(),
            ],
        )

        # A message from the calculator starts a new timeout
        self.endpoint.receive_frame.return_value = (
            Address.GCN_BRIDGE,
            Address.BROADCAST,
            b"\xabTestClie",
        )
        await self.room._process_frame()
        async with timeout(1):
            while len(mock_timeout.mock_calls) < 5:
                await asyncio.sleep(0)

        self.assertEqual(
            mock_timeout.mock_calls[-3:],
            [
                call().__aexit__(None, None, None),
                call(60),
                call().__aenter__(),
            ],
        )

    async def test_text_message(self):
        (calc, task) = await self.room._add_calculator(Address.GCN_BRIDGE, "Burns")

        self.endpoint.receive_frame.return_value = (
            Address.GCN_BRIDGE,
            Address.BROADCAST,
            b"\xadBurns:ahoy-hoy",
        )
        await self.room._process_frame()

        self.room.on_message.assert_awaited_once_with(
            calc,
            TextMessage(
                source=Address.GCN_BRIDGE,
                destination=Address.BROADCAST,
                nickname="Burns",
                text="ahoy-hoy",
            ),
        )

    async def test_unrecognized_message(self):
        self.endpoint.receive_frame.return_value = (
            Address.GCN_BRIDGE,
            Address.BROADCAST,
            b"\x42THIS IS NOT A CHAT MESSAGE",
        )
        await self.room._process_frame()

        self.assertNotIn(Address.GCN_BRIDGE, self.room.calculators)

    async def test_manage(self):
        await self.room._add_calculator(Address.GCN_BRIDGE, "TinyTim")

        with patch.object(
            self.room, "_process_frame", autospec=True
        ) as mock_process_frame:
            mock_process_frame.side_effect = asyncio.CancelledError()
            with self.assertRaises(asyncio.CancelledError):
                await self.room.manage()

        self.assertEqual(self.room.calculators, {})

    async def test_send_message(self):
        src = Address(b"\x01\x23\x45\x67\x89")
        message = TextMessage(
            source=src,
            destination=Address.BROADCAST,
            nickname="Tari",
            text="This is a message",
        )
        await self.room.send_message(message)
        self.room.endpoint.send_frame.assert_awaited_once_with(
            src=src, dest=Address.BROADCAST, payload=bytes(message)
        )

    async def test_connect_local_client(self):
        addr = Address.random()
        with patch.object(self.room, "send_message") as mock_send_message:
            async with self.room.connect_client("Nixon", addr) as c:
                await c.send_message("AroOoOoOo")

        # Expected messages were sent into the room
        self.assertEqual(
            mock_send_message.mock_calls,
            [
                call(
                    PingMessage(
                        source=addr, destination=Address.BROADCAST, nickname="Nixon"
                    )
                ),
                call(
                    TextMessage(
                        source=addr,
                        destination=Address.BROADCAST,
                        nickname="Nixon",
                        text="AroOoOoOo",
                    )
                ),
                call(DisconnectMessage(source=addr, destination=Address.BROADCAST)),
            ],
        )

        # The background pinging task is stopped
        async with asyncio.timeout(1):
            with self.assertRaises(asyncio.CancelledError):
                await c.ping_task

    async def test_local_client_announces_presence(self):
        """LocalClient runs _announce_presence to announce itself."""
        addr = Address.random()
        c = self.room.connect_client("Merkel", addr)

        with (
            patch.object(
                c, "_announce_presence", autospec=True
            ) as mock_announce_presence,
            patch.object(self.room, "send_message"),
        ):
            async with c, asyncio.timeout(1):
                while c.ping_task is None:
                    await asyncio.sleep(0)

        mock_announce_presence.assert_called_once()

    async def test_local_client_announce_presence_pings_periodically(self):
        """
        LocalClient._announce_presence periodically sends pings.

        This works in concert with the test that _announce_presence is run
        to check that clients will periodically send pings when connected.
        """
        lc = LocalClient(
            room=Mock(spec=Room), address=Address.random(), nickname="Merkel"
        )
        with (
            patch.object(lc, "_send_ping", autospec=True) as mock_send_ping,
            patch("asyncio.sleep", autospec=True) as mock_sleep,
        ):
            mock_send_ping.side_effect = [DEFAULT, asyncio.CancelledError()]

            with self.assertRaises(asyncio.CancelledError):
                async with asyncio.timeout(1):
                    await lc._announce_presence()

        # The task waited for a bit, then sent a ping, waited again and was
        # cancelled on the second ping.
        mock_sleep.assert_has_awaits([call(5), call(5)])
        mock_send_ping.assert_has_awaits([call(), call()])


class MessageEncodingTests(TestCase):
    def test_encode_ping(self):
        self.assertEqual(
            bytes(
                PingMessage(
                    source=Address.GCN_BRIDGE,
                    destination=Address.BROADCAST,
                    nickname="Dromar",
                )
            ),
            b"\xabDromar\0\0",
        )

    def test_encode_disconnect(self):
        self.assertEqual(
            bytes(
                DisconnectMessage(
                    source=Address.GCN_BRIDGE, destination=Address.BROADCAST
                )
            ),
            b"\xac",
        )

    def test_encode_text(self):
        self.assertEqual(
            bytes(
                TextMessage(
                    source=Address.GCN_BRIDGE,
                    destination=Address.BROADCAST,
                    nickname="Nixon",
                    text="I am not a crook",
                )
            ),
            b"\xadNixon:I am not a crook",
        )
