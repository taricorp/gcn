import asyncio
import unittest
from io import BytesIO
from logging import Logger, NullHandler
from unittest import mock
from unittest.mock import Mock, MagicMock, AsyncMock

from gcn.hub import GcnHub, ServerConfig, Client, VirtualHub


class ServerTestCase(unittest.IsolatedAsyncioTestCase):
    _server: asyncio.Server
    server_address: tuple[str, int]

    async def asyncSetUp(self):
        self.hub = GcnHub([ServerConfig(host="localhost", port=None)])

        created = asyncio.Event()

        async def server_ready(server):
            self._server = server
            self.server_address = server.sockets[0].getsockname()
            created.set()

        self._server_task = asyncio.create_task(self.hub.run_async(server_ready))
        await created.wait()

    async def asyncTearDown(self):
        self._server_task.cancel()
        self._server.close()
        # This can hang if the server isn't closing its transmit sides of the
        # sockets, and that's very difficult to debug. :)
        await self._server.wait_closed()

    class WrappedClient:
        """
        Wraps a connection to conveniently read from or write data to the server.
        """

        r: asyncio.StreamReader
        w: asyncio.StreamWriter

        def __init__(self, server_address):
            self.server_address = server_address

        async def __aenter__(self):
            host, port, *_ = self.server_address
            r, w = await asyncio.open_connection(host, port)
            self.r = r
            self.w = w
            return self

        async def __aexit__(self, ext_t, exc_v, exc_tb):
            self.w.close()
            await self.w.wait_closed()

        async def write(self, data):
            return self.w.write(data)

        async def readexactly(self, n):
            return await self.r.readexactly(n)

        async def read(self, n=-1):
            return await self.r.read(n)

    def client(self):
        return self.WrappedClient(self.server_address)

    @staticmethod
    def build_message(ty: str, payload: bytes) -> bytes:
        ty_b = ty.encode("ascii")
        assert len(ty_b) == 1
        return len(payload).to_bytes(2, byteorder="little") + ty_b + payload

    @classmethod
    def build_join(cls, hub: str, name: str) -> bytes:
        hub_b = hub.encode("ascii")
        name_b = name.encode("ascii")
        return cls.build_message(
            "j", len(hub_b).to_bytes(1) + hub_b + len(name_b).to_bytes(1) + name_b
        )

    async def join_hub(self, hub: VirtualHub, client: WrappedClient, client_name: str):
        await client.write(ServerTestCase.build_join(hub.name, client_name))

        have_clients = lambda: set(c.name for c in hub.clients)
        try:
            async with asyncio.timeout(2):
                while client_name not in have_clients():
                    await asyncio.sleep(0)
        except TimeoutError:
            self.assertIn(
                client_name,
                have_clients(),
                "Timed out waiting for client to be connected to virtual hub",
            )
            raise

    async def test_actual_broadcast(self):
        """A real server relays a broadcast from one client to another"""
        async with self.client() as c1, self.client() as c2:
            # Connect two clients to a virtual hub
            vhub = self.hub.get_virtual_hub("TestHub")
            await self.join_hub(vhub, c1, "Client1")
            await self.join_hub(vhub, c2, "Client2")

            # Broadcast some data from c1
            broadcast_message = self.build_message("b", b"Hello, friends!")
            await c1.write(broadcast_message)
            # c2 should receive the same message
            async with asyncio.timeout(2):
                relayed_message = await c2.read(200)
            self.assertEqual(relayed_message, broadcast_message)

    async def test_actual_unicast(self):
        """Unicast messages are delivered to the correct client"""
        async with self.client() as c1, self.client() as c2:
            vhub = self.hub.get_virtual_hub("UnicastHub")
            await self.join_hub(vhub, c1, "Client1")
            await self.join_hub(vhub, c2, "Client2")

            # Attach a calculator to client2
            await c2.write(self.build_message("c", b"C100000000"))
            # Wait until the server notes that calculator
            [client] = [c for c in vhub.clients if c.name == "Client2"]
            async with asyncio.timeout(2):
                while "C100000000" not in client.calculators:
                    await asyncio.sleep(0)

            # Send a unicast message to the calculator on client2 and it should
            # be relayed to that client only.
            unicast_message = self.build_message("f", b"??\xc1\0\0\0\0??")
            await c1.write(unicast_message)
            recv = await c2.readexactly(len(unicast_message))
            self.assertEqual(recv, unicast_message)


class ClientTestCase(unittest.IsolatedAsyncioTestCase):
    def createClient(self):
        read = MagicMock(
            **{
                "readexactly": AsyncMock(name="StreamReader.readexactly"),
            }
        )
        write = MagicMock(
            **{
                "write": Mock(name="StreamWriter.write"),
                "drain": AsyncMock(name="StreamWriter.drain"),
                "get_extra_info.return_value": ("example.com", 23456),
                "wait_closed": AsyncMock(name="StreamWriter.wait_closed"),
            }
        )
        return Client(self.hub, self.logger, read, write)

    def setUp(self):
        self.hub = GcnHub([])
        self.logger = Logger("tests.test_gcnhub")
        self.logger.addHandler(NullHandler())
        self.client = self.createClient()

    async def test_leaves_hub_on_close(self):
        """
        Using a Client as a context manager, it leaves its virtual hub on exit.
        """
        hub = VirtualHub("TestingHub")

        async with self.client:
            self.client.virtual_hub = hub
            hub.add(self.client)

        self.assertIsNone(self.client.virtual_hub)
        self.assertNotIn(self.client, hub.clients)

    async def test_join_hub(self):
        await self.client.handle_join_message(BytesIO(b"\x07CoolHub" b"\x0aLameClient"))

        self.assertEqual(self.client.name, "LameClient")
        self.assertEqual(self.hub.get_virtual_hub("CoolHub"), self.client.virtual_hub)
        self.assertIn(self.client, self.client.virtual_hub.clients)

    async def test_join_different_hub(self):
        await self.client.handle_join_message(BytesIO(b"\x01A\x01N"))

        with self.assertLogs(self.logger) as l:
            await self.client.handle_join_message(BytesIO(b"\x01B\x01Q"))

        # The client moved to the new hub
        self.assertIn(self.client, self.hub.get_virtual_hub("B").clients)
        self.assertNotIn(self.client, self.hub.get_virtual_hub("A").clients)

        # Messages were logged, with client info
        self.assertEqual(
            l.output,
            [
                (
                    """INFO:tests.test_gcnhub:client "N"@('example.com', 23456): """
                    "Renaming client to Q"
                ),
                (
                    """WARNING:tests.test_gcnhub:client "Q"@('example.com', 23456): """
                    "Joining hub B, but already connected to hub A"
                ),
                (
                    """INFO:tests.test_gcnhub:client "Q"@('example.com', 23456): """
                    "Joining hub B"
                ),
            ],
        )

    async def test_add_calculator(self):
        await self.client.handle_calculator_message(BytesIO(b"DEADBEEF01"))
        self.assertIn("DEADBEEF01", self.client.calculators)

    async def test_broadcast(self):
        vhub = self.hub.get_virtual_hub("BroadcastHub")
        vhub.add(self.client)
        self.client.virtual_hub = vhub
        self.client.write = MagicMock()

        other_client = self.createClient()
        vhub.add(other_client)
        other_client.virtual_hub = vhub

        await self.client.handle_broadcast_message(
            BytesIO(b"Hello, world! (This is not a valid CN2 frame.)")
        )

        # The message was sent to the other client but not the client that
        # sent us the broadcast.
        other_client.writer.write.assert_called_once_with(
            b"\x2e\x00bHello, world! (This is not a valid CN2 frame.)"
        )
        other_client.writer.drain.assert_awaited_once_with()
        self.client.write.assert_not_called()

    async def test_unicast(self):
        # Connect the test client to a hub
        vhub = self.hub.get_virtual_hub("H")
        self.client.virtual_hub = vhub
        vhub.add(self.client)

        # Connect another client with a calculator that we'll send to
        other_client = self.createClient()
        other_client.virtual_hub = vhub
        vhub.add(other_client)
        other_client.calculators.add("ABCDE12345")

        # This is (probably) actually a valid CN2 frame, from 0123456789
        # to ABCDE12345
        frame = b"..\xab\xcd\xe1\x23\x45\x01\x23\x45\x67\x89\x07\x00PAYLOAD\x0a\x02"
        await self.client.handle_unicast_message(BytesIO(frame))

        # The frame was forwarded to the other client
        other_client.writer.write.assert_called_once_with(b"\x17\x00f" + frame)
        self.client.writer.write.assert_not_called()

    async def patch_and_dispatch_client(self, ty: bytes, handler: str):
        self.client.reader.readexactly.side_effect = [b"\x01\x00", ty, b"!"]

        with mock.patch.object(Client, handler) as mock_handler:
            await self.client.handle_message()
        mock_handler.assert_called_once()
        ((bio,), _) = mock_handler.call_args
        self.assertEqual(bio.getvalue(), b"!")

    async def test_dispatch_messages(self):
        await self.patch_and_dispatch_client(b"j", "handle_join_message")
        await self.patch_and_dispatch_client(b"c", "handle_calculator_message")
        await self.patch_and_dispatch_client(b"b", "handle_broadcast_message")
        await self.patch_and_dispatch_client(b"f", "handle_unicast_message")
